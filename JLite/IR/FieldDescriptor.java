package IR;

/**
 * Field Descriptor
 *
 * Describes the field
 *
 */

public class FieldDescriptor extends Descriptor
{
	private TypeDescriptor type;
	private int value;
	private boolean initialized;

	public FieldDescriptor(String name)
	{
		super(name);
		this.initialized = false;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getType()
	{
		return type.toString();
	}
	public void setType(String input)
	{
		//System.out.println(input);
		type = new TypeDescriptor(input);
	}
	public int getValue()
	{
		return value;
	}
	public void setValue(int value)
	{
		this.value = value;
		this. initialized = true;
	}
	public boolean isInitialized()
	{
		return initialized;
	}
	public String toString()
	{
		return type.toString() + " " + name;
	}
}
