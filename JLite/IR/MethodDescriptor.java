package IR;
import IR.Tree.*;
/*
 * Method Descriptor
 *
 * Used to describe methods
 *
 */

public class MethodDescriptor extends Descriptor 
{
	private TypeDescriptor returnType;
	private SymbolTable parameters;
	private TreeNode method_body;

	public MethodDescriptor(String name)
	{
		super(name);
		returnType = new TypeDescriptor("temp");
	}
	public String getName()
    {
        return name;
    }
    public void setName(String newName)
    {
        this.name = newName;
    }
	public void setReturnType(String type)
	{
		//System.out.println(type + "in method descriptor");
		returnType.setType(type);
	
	}
	public TypeDescriptor getReturnType()
	{
		return returnType;
	}
	public void setParameters(SymbolTable parameters)
	{
		this.parameters = parameters;
	}
	public SymbolTable getParameters()
	{
		return parameters;
	}
	public void setAST(TreeNode method_body)
	{
		this.method_body = method_body;
	}
	public TreeNode getAST()
	{
		return method_body;
 	}
}	

