package IR.Tree;

public class NameNode extends ExpressionNode
{
	String name;

	public NameNode (String _s)
	{
		name = _s;
	}
	
	public String toString()
	{
		return getName();
	}

	public String getName()
	{
		return name;
	}
}

