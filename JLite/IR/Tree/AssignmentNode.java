package IR.Tree;

public class AssignmentNode extends ExpressionNode {
	private TreeNode lhs;
	private TreeNode rhs;
	public AssignmentNode(TreeNode lhs, TreeNode rhs)
	{
		this.lhs = lhs;
		this.rhs = rhs;
	}
	public TreeNode getLHS()
	{
		return lhs;
	}
	public TreeNode getRHS()
	{
		return rhs;
	}
	public void setRHS(TreeNode _r)
	{
		rhs = _r;
	}
	public void setLHS(TreeNode _l)
	{
		lhs = _l;
	}
	public String toString()
	{
		return lhs.toString() + " = " + rhs.toString();
	}
}
