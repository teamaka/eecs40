package IR.Tree;

public class IfNode extends BlockStatementNode
{
	Boolean trueOrFalse;
	BlockNode lha;
	BlockNode rha;
	Operation op;
	public IfNode(Boolean set, BlockNode lha, BlockNode rha, Operation op)
	{
		this.trueOrFalse = set;
		this.lha = lha;
		this.rha = rha;
		this.op = op;
	}
	public Boolean getTrueFalse()
	{
		return trueOrFalse;
	}
	public void setTrueFalse(Boolean set)
	{	
		trueOrFalse = set;
	}
	public BlockNode getLha()
	{
		return lha;
	}
	public BlockNode getRha()
	{
		return rha;
	}
	public void setLha(BlockNode bn)
	{
		lha = bn;
	}
	public void setRha(BlockNode bn)
	{
		rha = bn;
	}
	public Operation getOp()
	{
		return op;
	}
	public void setOp()
	{
		this.op = op;
	}
}
