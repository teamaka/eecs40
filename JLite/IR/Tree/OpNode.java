package IR.Tree;

/*
 *	OpNode
 *
 *	Contains information about a operation
 *
 */

public  class OpNode extends ExpressionNode
{
	private TreeNode left;
	private TreeNode right;
	private Operation op;
	public OpNode(TreeNode _l, TreeNode _r, Operation _o)
	{
		left = _l;
		right = _r;
		op = _o;
	}
	public TreeNode getRight()
	{
		return right;
	}
	public TreeNode getLeft()
	{
		return left;
	}
	public Operation getOp()
	{
		return op;
	}
	public String toString()
	{
		if (right == null)
		{
			return op + " " + left;
		}
		else
		{
			return "(" + left + " " + op + " " + right + ")";
		}
	}
	
}

