package IR.Tree;
import Parse.*;

public class BuildAST {
  public static final String bsl = "block_statement_list";
	
	
  public BuildAST() {
	  
  }

  public TreeNode buildAST(ParseNode pn) {
    String label=pn.getLabel();
    ParseNodeVector pnv=pn.getChildren();
    if(label.equals("body"))
    {
    	if(pn.getFirstChild().getLabel().equals("empty"))
    	{
    		return null;
    	}
    	else
    	{
    		return buildAST(pn.getChild(bsl));
    	}
    	
    }
    else if(label.equals(bsl))
    {
    	// only considering expression child
    	return buildAST(pn.getChild("expression"));
    }
    else if(label.equals("expression"))
    {
    	//only considering assignment child
    	return buildAST(pn.getChild("assignment"));
    }
    else if(label.equals("assignment"))
    {
    	return buildAST(pn.getChild("args"));
    }
    else if(label.equals("args"))
    {
    	TreeNode left = buildAST(pnv.elementAt(0));
    	TreeNode right = buildAST(pnv.elementAt(1));
    	return new AssignmentNode(left, right);
    }
    else if(label.equals("name"))
    {
    	// name has child identifier. identifier has child with name of id
    	return new NameNode(pn.getFirstChild().getFirstChild().getLabel());
    }
    else if (label.equals("literal")) {
      ParseNode c=pn.getChild("integer");
      return new LiteralNode(((Integer)c.getLiteral()).intValue());
    } else if (label.equals("add")) {
      TreeNode left = buildAST(pnv.elementAt(0));
      TreeNode right= buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.ADD));
    } else if (label.equals("sub")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.SUB));
    } else if (label.equals("mult")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.MULT));
    } else if (label.equals("div")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.DIV));
    } else if (label.equals("div")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.DIV));
    } else if (label.equals("not")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      return new OpNode(left,null,new Operation(Operation.NOT));
    } else if (label.equals("comp_lt")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.LT));
    } else if (label.equals("comp_gt")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.GT));
    } else if (label.equals("equal")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.EQ));
    } else if (label.equals("not_equal")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.NEQ));
    } else if (label.equals("bitwise_and")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.BIT_AND));
    } else if (label.equals("bitwise_or")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.BIT_OR));
    } else if (label.equals("bitwise_xor")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.BIT_XOR));
    } else if (label.equals("logical_and")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.LOG_AND));
    } else if (label.equals("logical_or")) {
      TreeNode left=buildAST(pnv.elementAt(0));
      TreeNode right=buildAST(pnv.elementAt(1));
      return new OpNode(left,right,new Operation(Operation.LOG_OR));
    } else {
      throw new Error();
    }
  }


}
