package IR.Tree;

public class LiteralNode extends ExpressionNode
{
	int value;
	public LiteralNode (int _v)
	{
		value = _v;
	}
	
	public String toString() 
	{
		return String.valueOf(value);
	}
}

