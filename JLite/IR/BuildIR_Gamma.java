package IR;
import Parse.*;
import IR.Tree.*;

public class BuildIR11111S {
	
	public static final String cu    = "compilation_unit";
	public static final String tdl   = "type_declaration_list";
	public static final String cd    = "class_declaration";
	public static final String cb    = "classbody";
	public static final String cbdl  = "class_body_declaration_list";
	public static final String fd    = "field_declaration";
	public static final String md    = "method_declaration";
	public static final String vd    = "variable_declarator";
	
	public static final String body = "body";
	public static final String method_declarator = "method_declarator";
        public static final String method_header = "method_header";
        public static final String name = "name";
        public static final String returntype = "returntype";
        public static final String formal_parameter = "formal_parameter";
        public static final String formal_parameter_list = "formal_parameter_list";
        public static final String parameter = "parameter";
	public static final String type = "type";


	SymbolTable class_table = new SymbolTable();
	
	public void ParseRoot(ParseNode pn)
	{
		//System.out.println(pn.getLabel());
		if(pn.getLabel().equals(cu))
		{
			CreateIR(pn.getChild(tdl).getChild(cd), class_table);
		}
		
	}
	
	public void CreateIR(ParseNode pn, SymbolTable table)
	{
		String label = pn.getLabel();
		if(label.equals(cd))
		{
			String name = pn.getChild("name").getFirstChild().getLabel();
			String parent = pn.getChild("super").getFirstChild().getLabel(); 
			//parent is empty if super class dne
			ClassDescriptor class_1 = new ClassDescriptor(name);
			//class_1.setParent();
			//ParseTree(pn.getChild(cb), table);
			ParseNodeVector pnv = pn.getChild(cb).getChild(cbdl).getChildren();
			for(int i = 0; i < pnv.size(); i++)
			{
				if(pnv.elementAt(i).getLabel().equals("field"))
				{
					class_1.setFields(CreateTable(pnv.elementAt(i), class_1.getFields()));
				}
				else if(pnv.elementAt(i).getLabel().equals("method"))
				{
					//getMethods instead
					class_1.setMethods(CreateTable(pnv.elementAt(i), class_1.getMethods()));
				}
			}
			class_table.add(class_1);
			System.out.println(class_table);
		}
	}
	
	private SymbolTable CreateTable(ParseNode pn, SymbolTable table)
	{
		String label = pn.getLabel();
		//ParseNodeVector pnv = pn.getChildren();
		FieldDescriptor field = new FieldDescriptor("temp");
		MethodDescriptor method = new MethodDescriptor("temporary");
		if(label.equals("field"))
		{
			field = CreateField(pn.getChild(fd), field);
			//System.out.println(field.toString());
			table.add(field);
		}
		else if (label.equals("method"))
		{
			//Create method return method descriptor
			//add method descriptor to method table 
			method = CreateMethod(pn.getChild(md), method);
			table.add(method);
		}
		System.out.println(table);
		return table;
		
	}

        private MethodDescriptor CreateMethod(ParseNode pn, MethodDescriptor method)
        {
		String label = pn.getLabel();
                ParseNode first_child = pn.getFirstChild();
                ParseNodeVector pnv = pn.getChildren();
		TreeNode tree = new TreeNode();
     		FieldDescriptor parameter = new FieldDescriptor("temp");
		SymbolTable ParameterTable = new SymbolTable(); 
		//System.out.print("MD");
		//System.out.println(label);
		if(label.equals(md))            
		{
                        for(int i = 0; i < pnv.size(); i++)
                        {
                              CreateMethod(pnv.elementAt(i), method);
                        }
                }
		else if(label.equals(body))
		{
			method.setAST(CreateAST(tree, first_child));
			return method;
		}
		else if (label.equals(method_declarator))
		{
			for(int i = 0; i < pnv.size(); i++)
                        {
                        	//System.out.println("Count");
				CreateMethod(pnv.elementAt(i), method);
                        }
		}
		else if (label.equals(method_header))
		{
			//System.out.println("Count");
			 for(int i = 0; i < pnv.size(); i++)
                        {
                                //System.out.println("Count");
                                CreateMethod(pnv.elementAt(i), method);
                        }
                        
		}
		else if(label.equals(name))
		{
			method.setName(first_child.getLabel());
			return method;
		}
		else if(label.equals(returntype))
		{
			//System.out.println(first_child.getLabel());
			
			if(first_child.getLabel().equals("type"))
			{
				//System.out.println(first_child.getFirstChild().getLabel() + "in BuildIR");
				method.setReturnType(first_child.getFirstChild().getLabel());
			}
			System.out.println("return type of " + method.getSymbol() + " is " + method.getReturnType().toString());
			return method;
		}
		else if(label.equals("formal_parameter")) 
		{
			//System.out.println("FP");
			ParameterTable.add(CreateParameter(pn, parameter));
			method.setParameters(ParameterTable);
			System.out.println("printing parameters " + ParameterTable);
			return method;
		}
		else if(label.equals(formal_parameter_list))
		{
			//System.out.println("in formal_parameter_list");
			for(int i = 0; i < pnv.size(); i++)
			{
				//System.out.println("in md, passing " + pnv.elementAt(i).getLabel());
		        	CreateMethod(pnv.elementAt(i), method);
			}
		}
		else if(label.equals("parameters")) 
		{
			CreateMethod(first_child, method);
		}
		else
                {
                        throw new Error();
                }
        return method;
        }

	private FieldDescriptor CreateParameter(ParseNode pn, FieldDescriptor field)
	{
		String label = pn.getLabel();
	        ParseNode first_child = pn.getFirstChild();
	        ParseNodeVector pnv = pn.getChildren();
		if(label.equals("formal_parameter"))            
	 	{
			for(int i = 0; i < pnv.size(); i++)
                        {
                		//System.out.println("Entered Create Parameter");	
		              	CreateParameter(pnv.elementAt(i), field);
                        }
	        }
		else if(label.equals("name"))
		{
			field.setName(first_child.getLabel());
			return field;
		}
		else if(label.equals("type"))
		{
			//field.setType(first_child.getLabel());
			CreateField(pn, field);
			return field;
		}
		else if (label.equals("single"))
		{
                        field.setName(first_child.getLabel());
                        return field;		
		}
		else
	        {
	        	throw new Error();
	        }
		return field;
	}

	private FieldDescriptor CreateField(ParseNode pn, FieldDescriptor field)
	{
		//int count = 0;
		String label = pn.getLabel();
		ParseNode first_child = pn.getFirstChild();
		ParseNodeVector pnv = pn.getChildren();
		if(label.equals(fd))
		{
			for(int i = 0; i < pnv.size(); i++)
			{
				//System.out.println("element at " + i + " is: " + pnv.elementAt(i).getLabel());
				CreateField(pnv.elementAt(i), field);
			}
		}
		else if(label.equals("type"))
		{
			// if type is int
			if(first_child.getLabel().equals(type))
			{
				//System.out.println("first_child of firstchild of type is:" + first_child.getFirstChild().getLabel());
				field.setType(first_child.getFirstChild().getLabel());
				return field;
			}
			else if(first_child.getLabel().equals("class"))
			{
				CreateField(first_child.getFirstChild(), field);
				return field;
			}
			if(first_child.getLabel().equals("int"))
			{
				field.setType(first_child.getLabel());
				return field;
			}
			else
			{
				System.out.println("Bypassed both cases");
			}
		}
		else if(label.equals("name"))
		{
			CreateField(first_child, field);
		}
		else if(label.equals("identifier"))
		{
			field.setType(first_child.getLabel());
			return field;
		}
		else if(label.equals("variables"))
		{
			CreateField(first_child, field);
		}
		else if(label.equals(vd))
		{
			CreateField(first_child,field);
		}
		else if(label.equals("single"))
		{
			field.setName(first_child.getLabel());
			return field;
		}
		else
		{
			throw new Error();
		}
		//System.out.println("count" + count + " " + field.getType() + " " + field.getName());
		return field;
	}
	private TreeNode CreateAST(TreeNode tree, ParseNode pn)
	{
		return tree;
	}
}
