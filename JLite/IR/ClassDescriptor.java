package IR;

/**
 * Class Descriptor
 *
 * Used to describe classes
 *
 */

public class ClassDescriptor extends Descriptor
{
	private ClassDescriptor parent;
	private SymbolTable field_table;
	private SymbolTable method_table;

	public ClassDescriptor(String name)
	{
		super(name);
		field_table = new SymbolTable();
		method_table = new SymbolTable();
	}
	public String getName()
	{
		return name;
	}
	public void setName(String newName)
	{
		this.name = newName;
	}
	public void setFields(SymbolTable table)
	{
		this.field_table = table;
	}
	public SymbolTable getFields()
	{
		return field_table; 
	}
	public void setMethods(SymbolTable table)
	{
		this.method_table = table;
	}
	public SymbolTable getMethods()
	{
		return method_table;
	}
	public void setParent(ClassDescriptor input)
	{
		this.parent = input;
	}
	public ClassDescriptor getParent()
	{
		return parent;
	}
	public String toString()
	{
		if(parent == null)
			return "class " + name + "{\n";
		else
			return "class " + name + " extends " + parent.toString() + "{\n";
	}
}
