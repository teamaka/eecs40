package IR;

/*
 * Type Descriptor
 *
 * Used to describe type
 *
 */

public class TypeDescriptor extends Descriptor
{
	//private String name;
	private enum Types
	{
		INT_TYPE, VOID_TYPE, CLASS_TYPE
	};
	Types type;
	//private ClassDescriptor type_class;
	private boolean isClass;

	public TypeDescriptor(String name)
	{
		super(name);
		setType(name);
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
		setType(name);
	}
	public void setType(String input)
	{
		//System.out.println(input + "in type descriptor");
		if(input.equals("int"))
			type = Types.INT_TYPE;
		else if(input.equals("void"))
			type = Types.VOID_TYPE;
		else
			type = Types.CLASS_TYPE;
	}
	public String toString()
	{
		if(type == Types.INT_TYPE)
			return "int";
		else if(type == Types.VOID_TYPE)
			return "void";
		else
			return name;
	}
}
	
